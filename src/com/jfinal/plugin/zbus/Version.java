package com.jfinal.plugin.zbus;

public class Version {

    public final static int MajorVersion = 0;
    public final static int MinorVersion = 11;
    public final static int RevisionVersion = 0;

    public static final String VERSION = Version.MajorVersion + "." + Version.MinorVersion + "." + Version.RevisionVersion;
    public static final String RELEASE_AT = "2018-01-03";
    public static final String RELEASE_NOTE = "升级到zbus0.11.*";

    private Version() {
    }
}
